package com.example.demo.controller;

import com.example.demo.service.FileService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/file")
@AllArgsConstructor
public class Controller {

    FileService fileService;

    @PostMapping(path = "/upload")
    public void readFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        fileService.processFile(multipartFile);
    }
}
