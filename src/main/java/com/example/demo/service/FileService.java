package com.example.demo.service;

import com.example.demo.component.Processing;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class FileService {

    Processing processing;

    public void processFile(MultipartFile multipartFile) throws IOException {
        InputStream inputStream = multipartFile.getInputStream();
        readFileRecords(inputStream);
        
    }

    private void readFileRecords(InputStream inputStream) {
        List<String>  stringList = new ArrayList<>();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){
            String line;
            while ((line = reader.readLine())!= null){
                stringList.add(line);
            }
            loopOverArray(stringList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loopOverArray(List<String> list){
        if(!list.isEmpty()){
            list.remove(0);
            List<Integer> convertedValues = list.stream().map(e-> processing.processLine(e)).collect(Collectors.toList());
            for (int i=0; i<convertedValues.size();i++){
                log.info("Case #{}: {}", i,convertedValues.get(i));
            }
        }
    }


}
