package com.example.demo.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Processing {
    public Integer processLine(String line) {
        try{
            Integer integer = Integer.parseInt(line);
            return loopInteger(integer);

        }catch (Exception e){
           throw new RuntimeException(e.getMessage());
        }
    }

    private static Integer loopInteger(Integer integer){
        while(!checkClean(integer)){
            integer--;
        }
        return integer;
    }

    private static boolean checkClean(Integer integer) {
        char[] array = String.valueOf(integer).toCharArray();
        for(int i=0;i<array.length-1;i++){
            if(array[i] > array[i+1]){
                return false;
            }
        }
        return true;
    }

}
