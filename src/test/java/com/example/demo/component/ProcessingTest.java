package com.example.demo.component;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ProcessingTest {

    @InjectMocks
    Processing processing;

    @Test
    void processLine() {
        String line = "1000";
        Integer expected = 999;
        Integer result = processing.processLine(line);
        assertThat(expected).isEqualTo(result);
    }

    @Test
    void processLineWithInvalidNumber() {
        String line = "1000erw";
        RuntimeException e = assertThrows(RuntimeException.class, ()-> processing.processLine(line));
        assertTrue(e.getMessage().contains(line));
    }
}