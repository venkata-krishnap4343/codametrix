package com.example.demo.service;

import com.example.demo.component.Processing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class FileServiceTest {

    @Mock
    Processing processing;

    @InjectMocks
    FileService fileService;

    @Test
    void processFile() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "test", MediaType.TEXT_PLAIN_VALUE, "124\n134".getBytes());
        fileService.processFile(file);
        verify(processing, never()).processLine("124");
        verify(processing).processLine("134");
    }

    @Test
    void processFileWithJustTheSize() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "test", MediaType.TEXT_PLAIN_VALUE, "124".getBytes());
        fileService.processFile(file);
        verify(processing, never()).processLine("124");
    }

    @Test
    void processFileWithNone() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "test", MediaType.TEXT_PLAIN_VALUE, "".getBytes());
        fileService.processFile(file);
        verify(processing, never()).processLine(anyString());
    }
}