# Codametrix Interview Question

This is micro-service that used gradle as a build tool.

You can rust the project and starts up in port 8080.

You can navigate to http://localhost:8080 where you can see a swagger page with one end point.

You can use the end point to upload flat files with data in it.

Once you upload the data and if has valid data in it, you can see a log in the console.

#Code:

In the code I used service class to read the content from string and assigned the content to a List.

Once I had the list I create a component (Processing) class which takes in the values and gives the closed clean values to it.

#Test Cases:

Both the Service class and the component classes are fully code covered. You can test it by running test with code coverage.